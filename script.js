const todoTask = document.querySelector(".input-todo");
const addTodoBtn = document.querySelector(".btn-add-todo");
let todoLists = document.querySelector(".todo--task-lists");

addTodoBtn.addEventListener("click", todoActionHandler);

//Event listnere function
function todoActionHandler() {
  let todoTaskValue = todoTask.value.trim();
  if (todoTaskValue) {
    //Add todo task
    addTodoHandler(todoTaskValue);
    editTodoHandler();

    //remove todo task
    removeTodoHandler();
  } else {
    alert("Please add some task!");
  }
}

todoTask.addEventListener("keypress", (e) => {
  if (e.key == "Enter") todoActionHandler();
});

//Adding todo
function addTodoHandler(curr_task) {
  todoLists.innerHTML += `<div class='todo-list-item'><span>${curr_task}</span> 


   <span>
   <i class="fa-solid fa-pen action-icons edit--icon"><i>
   <i class="fa fa-close action-icons del--icon"></i>
    
   </span></div>`;
  todoTask.value = "";
}

//Removing todo
function removeTodoHandler() {
  const allDelBtns = document.querySelectorAll(".del--icon");
  for (var i = 0; i < allDelBtns.length; i++) {
    allDelBtns[i].addEventListener("click", function () {
      this.closest(".todo-list-item").remove();
    });
  }
}

//edit todo
function editTodoHandler() {
  const allEditBtns = document.querySelectorAll(".edit--icon");
  for (var i = 0; i < allEditBtns.length; i++) {
    allEditBtns[i].addEventListener("click", function () {
      let curr_task = this.closest(".todo-list-item").firstChild;

      let prevEdit = curr_task.innerText;
      curr_task.innerHTML = `
      <input type='text'   id='edit-todo-task' />
      `;
      curr_task.firstElementChild.focus();

      let activeInput = document.getElementById("edit-todo-task");
      activeInput.value = prevEdit;
      if (activeInput) {
        activeInput.addEventListener("keypress", (e) => {
          if (e.key == "Enter") {
            curr_task.innerHTML = `<span>${activeInput.value}</span> `;
            activeInput.removeAttribute("id");
          }
        });
      }
    });
  }
}
